/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sumokung.ox;

import java.util.Scanner;

/**
 *
 * @author ASUS
 */
public class Game {

    Scanner kb = new Scanner(System.in);
    Player PlayerO;
    Player PlayerX;
    Table table;
    int row = 0, col = 0;
    
    public Game() {
        PlayerX = new Player('X');
        PlayerO = new Player('O');
        table = new Table(PlayerX, PlayerO);
    }

    public void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public void input() {

        while (true) {
            System.out.println("Please input Row Col:");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
            if (table.setRowCol(row, col)) {
                break;
            }
            table.showTable();
        };
    }

    public void showTurn() {
        System.out.println(table.getCurrentPlayer().getName() + " turn");
    }
    
    public void showBye() {
        System.out.println("Bye Bye!!");
    }

    public void run() {
        showWelcome();
        while (true) {
            table.showTable();
            showTurn();
            input();
            table.checkWin();
            table.switchPlayer();
            if(table.isFinish() == true){
                System.out.println(table.getWinner().getName() + " Win!!");
                showBye();
                break;
            }else{
                if(table.getTurn() == 9){
                    System.out.println("Draw!!");
                    showBye();
                    break;
                }
            }
        }
    }
}
