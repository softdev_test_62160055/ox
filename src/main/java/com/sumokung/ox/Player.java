/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sumokung.ox;

import java.io.Serializable;

/**
 *
 * @author ASUS
 */
public class Player implements Serializable{
    private char name;
    private int win;
    private int lose;
    private int draw;
        
    public Player(char name){
        this.name = name;
        this.win=0;
        this.lose=0;
        this.draw=0;
    }

    public char getName() {
        return name;
    }

    public void setName(char name) {
        this.name = name;
    } 
    public int getWin() {
        return win;
    }

    public void setWin(int win) {
        this.win = win;
    }

    public int getLose() {
        return lose;
    }

    public void setLose(int lose) {
        this.lose = lose;
    }

    public int getDraw() {
        return draw;
    }

    public void setDraw(int draw) {
        this.draw = draw;
    }
    public void addWin(){
        win++;
    }
    public void addLose(){
        lose++;
    }
    public void addDraw(){
        draw++;
    }

    @Override
    public String toString() {
        return "Player{" + "name=" + name + ", win=" + win + ", lose=" + lose + ", draw=" + draw + '}';
    }
    
}
