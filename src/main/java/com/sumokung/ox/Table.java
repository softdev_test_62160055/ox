/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sumokung.ox;

import java.io.Serializable;

/**
 *
 * @author ASUS
 */
public class Table implements Serializable{

    private char[][] table = {{'-', '-', '-'},
    {'-', '-', '-'},
    {'-', '-', '-'}};
    private Player PlayerX;
    private Player PlayerO;
    private Player currentPlayer;
    private int lastRow,lastCol;
    private boolean finish = false;
    private Player winner;
    private int turn=1;

    public Table(Player x, Player o) {
        this.PlayerX = x;
        this.PlayerO = o;
        this.currentPlayer = x;
    }

    public void showTable() {
        System.out.println(" 1 2 3");
        for (int row = 0; row < table.length; row++) {
            System.out.print(row + 1);
            for (int col = 0; col < table[row].length; col++) {
                System.out.print(table[row][col] + " ");
            }
            System.out.println();
        }
    }

    public boolean setRowCol(int row, int col) {
        if(isFinish()) return false;
        if (row < 3 && col < 3 && col >= 0 && row >= 0) {
            if (table[row][col] == '-') {
                table[row][col] = currentPlayer.getName();
                this.lastRow = row;
                this.lastCol = col;
                checkWin();
                increaseTurn();
                return true;
            }
            System.out.println("Cannot input try again!!");
            return false;
        }
        System.out.println("Number row or col is not have "
                        + " in table.Try again!!");
        return false;
    }
    
    public char getRowCol(int row,int col){
        return table[row][col];
    }
    
    public Player getCurrentPlayer(){
        return currentPlayer;
    }
    
    public void switchPlayer(){
        if(currentPlayer == PlayerX){
            currentPlayer = PlayerO;
        }else
            currentPlayer = PlayerX;
       
    }
    
    void increaseTurn(){
        turn++;
    }
    
    public void checkWin(){
        checkRow();
        checkCol();
        checkDiagonal();
        checkDraw();
    }
    void checkDraw(){
        if(turn==9 && winner == null){
           finish = true;
           setStatDraw();   
        }
    }
    
    void checkRow() {
        for (int col = 0; col < table.length; col++) {
            if (table[lastRow][col] != currentPlayer.getName()) {
                return;
            }
        }
        finish = true;
        winner = currentPlayer;
        setStatWinLose();
    }

    void checkCol() {
        for (int row = 0; row < table.length; row++) {
            if (table[row][lastCol] != currentPlayer.getName()) {
                return;
            }
        }
        finish = true;
        winner = currentPlayer;
        setStatWinLose();
    }

    void checkDiagonal() {
        checkBackslash();
        checkSlash();
    }

    void checkBackslash() {
        for (int row = 0,col = 0; row < table.length; row++,col++) {
            if (table[row][col] != currentPlayer.getName()) {
                return;
            }
        }
        finish = true;
        winner = currentPlayer;
        setStatWinLose();
    }

    void checkSlash() {
        int i = 0;
        for (int row = 2; row >= 0; row--) {
            if (table[row][i] != currentPlayer.getName()) {
                return;
            }
            i++;
        }
        finish = true;
        winner = currentPlayer;
        setStatWinLose();
    }
    private void setStatWinLose() {
        if (currentPlayer == PlayerO) {
            PlayerO.addWin();
            PlayerX.addLose();
        } else {
            PlayerO.addLose();
            PlayerX.addWin();
        }
    }
    
    private void setStatDraw(){
        PlayerO.addDraw();
        PlayerX.addDraw();
    }
    
    public boolean isFinish(){
        return finish;
    }
    
    public Player getWinner(){
        return winner;
    }
    
    public int getTurn(){
        return turn;
    }
    
    

}
